Beeckon's documentation.
########################

General information
*******************

Login
=====

To do this, send an email and password. If all goes well, you will get a token.

Request (shell):

.. code-block:: shell

    $curl -H "Content-Type: application/json" -X POST -d '{"username":"<EMAIL>","password":"<PASSWORD>"}' http://beeckon.swiss/api/api-token-auth/

<EMAIL> - *user mail from Beeckon site.*

<PASSWORD> - *user password from Beeckon site.*

Success response:

.. code-block:: python

    {
        "token": "<TOKEN>"
    }


<TOKEN> - *token, which gives permissions to manage user account.*

Error response:

.. code-block:: python

    {
        "non_field_errors": [
            "Unable to log in with provided credentials."
        ]
    }



Indices and tables
==================

* :ref:`search`

